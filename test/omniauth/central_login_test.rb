# frozen_string_literal: true

require "test_helper"

module Omniauth
  class CentralLoginTest < Minitest::Test
    def test_that_it_has_a_version_number
      refute_nil ::Omniauth::CentralLogin::VERSION
    end
  end
end
