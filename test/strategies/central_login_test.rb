# frozen_string_literal: true

require "test_helper"

module OmniAuth
  module Strategies
    class CentralLoginTest < Minitest::Test
      def test_that_it_is_a_strategy
        assert ::OmniAuth::Strategies::CentralLogin.new(nil).is_a?(OmniAuth::Strategies::OAuth2)
      end

      def test_token_id_verification
        strategy = OmniAuth::Strategies::CentralLogin.new(
          "a",
          "b",
          {client_options: {site: "https://example.com/oauth"}, scope: "openid email profile", response_type: "id_token"}
        )

        # strategy.validate_id_token("a")
      end
    end
  end
end
