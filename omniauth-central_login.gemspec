# frozen_string_literal: true

require_relative "lib/omniauth/central_login/version"

Gem::Specification.new do |spec|
  spec.name          = "omniauth-central_login"
  spec.version       = Omniauth::CentralLogin::VERSION
  spec.authors       = ["murb"]
  spec.email         = ["git@murb.nl"]

  spec.summary       = "A Central Login strategy for OmniAuth"
  spec.description   = "A companion gem to CentralLogin, a batteries included open source OAuth2 Provider based on
                        Rails, Doorkeeper & Devise, with simple user managment tools."
  spec.homepage      = "https://murb.nl/blog?tags=centrallogin"
  spec.license       = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/murb-org/omniauth-centrallogin"
  spec.metadata["changelog_uri"] = "https://gitlab.com/murb-org/omniauth-centrallogin/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  # Uncomment to register a new dependency of your gem
  spec.add_dependency "omniauth-oauth2", "~> 1.7"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
