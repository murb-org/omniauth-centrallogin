## [0.1.2]

- Parse (and return) ID token using JKT validation (using jwks); instead of requesting userinfo

## [0.1.1] - 2021-12-02

- Added full token id when returned

## [0.1.0] - 2021-11-25

- Initial release
