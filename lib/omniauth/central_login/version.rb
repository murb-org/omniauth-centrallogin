# frozen_string_literal: true

module Omniauth
  module CentralLogin
    VERSION = "0.1.2"
  end
end
