# frozen_string_literal: true

require "omniauth-oauth2"

module OmniAuth
  module Strategies
    # frozen_string_literal: true

    # A Central Login strategy for OmniAuth
    #
    # Configuring Omniauth:
    #
    #   Rails.application.config.middleware.use OmniAuth::Builder do
    #     provider :central_login, ENV['CENTRAL_LOGIN_CLIENT_ID'], ENV['CENTRAL_LOGIN_CLIENT_SECRET'], {
    #       scope: "openid email profile",
    #       client_options: {
    #         site: ENV['CENTRAL_LOGIN_URL']
    #       }
    #     }
    #   end
    #
    # Configuration for Devise (using omniauthable):
    #
    #   config.omniauth :central_login,
    #     Rails.application.secrets.central_login_id,
    #     Rails.application.secrets.central_login_secret,
    #     {client_options: {site: Rails.application.secrets.central_login_site}, scope: "openid email profile"}
    #
    class CentralLogin < OmniAuth::Strategies::OAuth2

      # /.well-known/openid-configuration
      option :name, "central_login"
      option :client_options, {site: ""}
      option :redirect_url
      option :scope, "openid"

      uid { raw_info["uid"].to_s }

      info do
        {
          name: raw_info["name"],
          email: raw_info["email"],
          email_verified: raw_info["email_verified"]
        }
      end

      extra do
        { raw_info: raw_info, id_token: id_token }
      end

      def id_token
        if options.response_type.to_s == "id_token"
          @id_token ||= access_token["id_token"]
        end
      end

      def validate_id_token(id_token)
        JWT.decode(id_token, nil, true, {
          algorithms: ["RS256"],
          jwks: jwks,
          iss: options.client_options[:site]
        })[0]
      end

      def raw_info
        return @raw_info if @raw_info

        if id_token
          @raw_info = validate_id_token(id_token)
        else
          @raw_info = access_token.get("/oauth/userinfo").parsed

          if @raw_info
            @raw_info["issuer"] = access_token
                                  .get("/.well-known/webfinger?resource=#{@raw_info["email"]}")
                                  .parsed["links"]
                                  .select { |a| a["rel"] == "http://openid.net/specs/connect/1.0/issuer" }[0]["href"]
          end
        end

        @raw_info

      rescue ::OAuth2::Error => e
        raise ::Omniauth::CentralLogin::Error, "Make sure you have 'openid' added as scope (OAuth2::error: #{e.message})"
      end

      private

      def jwks
        JSON.parse(Faraday::Connection.new(URI.join(options.client_options[:site], "oauth/discovery/keys")).get.body)
      end

      def callback_url
        options.redirect_url || (full_host + script_name + callback_path)
      end
    end
  end
end

OmniAuth.config.add_camelization "central_login", "CentralLogin"
