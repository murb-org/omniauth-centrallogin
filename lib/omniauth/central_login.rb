# frozen_string_literal: true

require_relative "central_login/version"
require_relative "strategies/central_login"

module Omniauth
  module CentralLogin
    class Error < StandardError; end
    # Your code goes here...
  end
end
