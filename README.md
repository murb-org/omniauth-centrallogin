# Omniauth::CentralLogin

A companion gem to [CentralLogin](https://gitlab.com/murb-org/central_login/), a batteries included open source OAuth2 Provider based on Rails, Doorkeeper & Devise, with simple user managment tools.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'omniauth-central_login'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install omniauth-central_login

## Usage

Configuring Omniauth:

    Rails.application.config.middleware.use OmniAuth::Builder do
      provider :central_login, ENV['CENTRAL_LOGIN_CLIENT_ID'], ENV['CENTRAL_LOGIN_CLIENT_SECRET'], {
        scope: "openid email profile",
        client_options: {
          site: ENV['CENTRAL_LOGIN_URL']
          },
        response_type: "id_token" # saves another round trip, but is optional
      }
    end

Configuration for Devise (using omniauthable):

    config.omniauth :central_login, Rails.application.secrets.central_login_id, Rails.application.secrets.central_login_secret, {client_options: {site: Rails.application.secrets.central_login_site}, scope: "openid email profile", response_type: "id_token"}

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome on GitLab at https://gitlab.com/murb-org/omniauth-central_login. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/murb-org/omniauth-central_login/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Omniauth::CentralLogin project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/murb-org/omniauth-central_login/blob/master/CODE_OF_CONDUCT.md).
